FROM node:10

EXPOSE 80

ADD start.sh start.sh

RUN chmod +x start.sh

CMD ./start.sh