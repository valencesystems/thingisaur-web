import React from 'react'
import Responsive from 'react-responsive';
import axios from 'axios';
import ReactPixel from 'react-facebook-pixel';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'

const Desktop = props => <Responsive {...props} minWidth={992} />;
const Mobile = props => <Responsive {...props} maxWidth={992} />;

var stripeSku;
var stripeButtonCount = 0;

export default class Checkout extends React.Component {

  constructor(props) {
    super(props);
    this.loadFormScript = this.loadFormScript.bind(this);
  }

  beginLoadingAnimation(){
    this.setState({loading : true});
  }

  componentDidMount(){

    stripeSku = this.props.stripeSku;

    document.getElementById('checkoutform').addEventListener("submit", this.beginLoadingAnimation, false);
    this.loadFormScript(function(){})

    var scope = this;
    document.addEventListener("buyForFriend", function (e) {
      scope.loadFormScript(function(){
        //window.StripeCheckout.__app.configurations.button0;
      })
    });
  }

  loadFormScript(onload){

    var stripeKey;
    if(process.env.REACT_APP_StripeMode === 'live'){
      stripeKey = process.env.REACT_APP_StripeLiveKey;
    } else stripeKey = process.env.REACT_APP_StripeTestKey;
  
    const script = document.createElement("script");
    script.src="https://checkout.stripe.com/checkout.js";
    script.dataset.key= stripeKey;
    script.className= "stripe-button";
    script.dataset.amount= this.props.amount;
    script.dataset.name="Thingisaur";
    script.dataset.billingAddress = true;
    script.dataset.shippingAddress = true;
    script.dataset.description= this.props.description;
    script.dataset.image="https://thingisaur-images.nyc3.digitaloceanspaces.com/stripe_icon.png";
    script.dataset.locale="auto";
    script.dataset.zipCode="true";
    
    let form = document.getElementById('checkoutform');
    form.submit = this.submitStripe; 

    script.onload = function(){
      onload();
      //window.StripeCheckout.open(window.StripeCheckout.__app.configurations.button0)
    }

    form.appendChild(script);
  }

  spinner() {
    var style = {"font-size": 48};
    return (
        <i class="fa fa-circle-o-notch" style={style}/>
    );
  }

  addToCart(e){
    ReactPixel.track( 'AddToCart',{})
    //document.getElementById('productBlurb').style.display = 'none'
    window.StripeCheckout.open(window.StripeCheckout.__app.configurations["button" + stripeButtonCount])
  }

  submitStripe(){

    document.getElementById('stripeButton').style.display = 'none'
    document.getElementById('descriptions').style.display = 'none'
    document.getElementById('loadingContainer').style.display = 'block'

    var serialize = require('form-serialize');
    var form = document.getElementById('checkoutform');
    var formdata = serialize(form, { hash: true });

    axios({
      method: 'post',
      url: process.env.REACT_APP_OrderUrl,
      data: {formdata, stripeSku},
      config: { headers: {'Content-Type': 'multipart/form-data' }}
    })
    .then(function (response) {
        //handle success
        document.getElementById('spinnerContainer').style.display = 'none'
        ReactPixel.track( 'OrderSubmitted',{})
        confirmAlert({
          title: 'Yes!',
          message: 'Your order has been placed. Your order ID ' + response.data + '. You\'ll also get a confirmation email with your order ID, so don\'t worry.',
          buttons: [
          {
            label: 'Buy Another!',
            onClick: () =>{
              window.location.reload();
              document.getElementById("checkoutform").reset(); 
              document.getElementById('stripeButton').style.display = 'block';
              document.getElementById('descriptions').style.display = 'block'
              document.getElementById('loadingContainer').style.display = 'none';
              
              var buyForFriend = new CustomEvent("buyForFriend", {});
              document.dispatchEvent(buyForFriend);
            }
          },
          {
            label: 'Show me Kittens!',
            onClick: () => window.location.href = "https://imgur.com/r/kittens"
          },]
        })
        //console.log(response);
    })
    .catch(function (response) {
        document.getElementById('spinnerContainer').style.display = 'none'
        //handle error
        //console.log(response);
        confirmAlert({
          title: 'Uh Oh...',
          message: 'It looks like a network error occured. Check your internet connection. Your card was not charged, don\'t worry.',
          buttons: [{
            label: 'Try Again!',
            onClick: () =>{
              window.location.reload();
              document.getElementById('stripeButton').style.display = 'block';
              document.getElementById('loadingContainer').style.display = 'none';
              document.getElementById('descriptions').style.display = 'block';
              document.getElementById("checkoutform").reset(); 

            }
          }]
        })
    });
  }

  render() {
    return (
      <div id="itemContainer">
        <script src="https://js.stripe.com/v3/"></script>
        <form id="checkoutform">
          <Desktop className="Item">
            <div id="loadingContainer">
              <div id="spinnerContainer">
                <i id="spinner" className="fa fa-circle-o-notch fa-spin Spinner" /><div className="robotoFont">One moment while we process your order.</div>
              </div>
            </div>     
            <div className="GetButton" type="button" id="stripeButton" onClick={this.addToCart}>
              Get me ${this.props.buttonAmmount}
            </div>
          </Desktop>

          <Mobile className="Item">
                <div id="loadingContainer">
                  <div id="spinnerContainer">
                    <i id="spinner" className="fa fa-circle-o-notch fa-spin Spinner" /><div className="robotoFont">One moment while we process your order.</div></div>
                  </div>
                  <div className="GetButton" type="button" id="stripeButton" onClick={this.addToCart}>
                    Get me ${this.props.buttonAmmount}
                  </div>
          </Mobile>
        </form>
      </div>
    )
  }
}