import React, { Component} from 'react';
import RetinaImage from 'react-retina-image';
import './App.css';

class Home extends Component {

  componentDidMount(){
    document.title = "Thingisaur"
  }

  render() {
    return (
      <div className="App">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossOrigin="anonymous"/>
        <header className="App-header">
          <div className="ThingisaurLogo">
            <RetinaImage src="./logo.png" />
          </div>
        </header>
        <div className="robotoFont aboutUs">
          <p><span className="hiFont">We love cool products</span></p>
          <div className="spacer"></div>
          <p>But there is so much junk out there to sift through!</p>
          <div className="spacer"></div>
          <p>We scour the online world for the things we love (and sometimes design them ourselves!). 
          And we're picky. 
          When we find great stuff, we think about who else might like them. 
          Then we bring it to them directly, with competitive pricing. 
          We hope you enjoy all the things as much as we do!</p>
          <div className="spacer"></div>
          <div>-Thingasaur Team</div>
        </div>
        <div className="spacerMore"></div>
        {/* <Map className="map"></Map> */}
      </div>
    );
  }
}

export default Home;
