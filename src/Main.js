import React, {Component} from 'react';
import Product from "./Product"
import axios from 'axios';

//console.log('REACT_APP_InfoUrl: ' + process.env.REACT_APP_InfoUrl)

var pid;
class Main extends Component {

     constructor(){
        super();
        this.state = {productdata: false}
    }

    componentDidMount(){
        pid = this.props.match.params.pid;
        this.getProductInfo(pid);   
    }

    getProductInfo(pid){
        axios({
        method: 'post',
            url: process.env.REACT_APP_InfoUrl,
            data: {"stripeSku": pid},
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then((response) => {
            this.setState({productReturned: response.data})
        })
        .catch(function (response) {
            //console.log(response);
        });
    }

    render() {
        var gotProduct = this.state.productReturned;
        return (
            <div>
               {gotProduct ? (
                    <Product productName={gotProduct.productName}
                            productBlurb={gotProduct.productBlurb}
                            stripeSku={gotProduct.stripeSku}
                            ammount={gotProduct.ammount}
                            buttonAmmount={gotProduct.buttonAmmount}
                            shippingAmmount={gotProduct.shippingAmmount}
                            description={gotProduct.description}
                            image_desktop={gotProduct.image_desktop}
                            image_mobile={gotProduct.image_mobile}
                    />
                ):(
                 <div></div> 
              )}
            </div>
        );
    }
}

export default Main;
    