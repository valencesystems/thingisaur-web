import React, { Component} from 'react';
import Responsive from 'react-responsive';
import ReactPixel from 'react-facebook-pixel';
import RetinaImage from 'react-retina-image'

import Checkout from './Checkout'
import './App.css';

const Desktop = props => <Responsive {...props} minWidth={992} />;
const Mobile = props => <Responsive {...props} maxWidth={992} />;

const hostname = window && window.location && window.location.hostname;
var imageServer;
if(hostname.indexOf("thingisaur.com") > -1){
  imageServer = "https://thingisaur-images.nyc3.digitaloceanspaces.com"
} else imageServer = "https://thingisaur-images.nyc3.digitaloceanspaces.com"

ReactPixel.init('1533897013382570');
ReactPixel.pageView();

class Product extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="ThingisaurLogo">
            <RetinaImage src="https://thingisaur-images.nyc3.digitaloceanspaces.com/logo.png" />
          </div>
        </header>

        <div>
          <div className="Checkout">
            
            <div id="descriptions">
              <div id="productName" className="productName"><h3>{this.props.productName}</h3></div>
              <div id="productBlurb" className="productBlurb">{this.props.productBlurb}</div>
            </div>
            
            <Checkout productName={this.props.productName} buttonAmmount={this.props.buttonAmmount} stripeSku={this.props.stripeSku} ammount={this.props.ammount} shippingAmmount={this.props.shippingAmmount} description={this.props.description}/>
          
          </div>

          <div>
            <Desktop className="ItemDesktop"><img src={imageServer + "/CorgiMugs/Corgi_ButtMug_08_Tablet.png"} alt=""/></Desktop>
            <Mobile className="ItemMobile"><img src={imageServer + "/CorgiMugs/Corgi_ButtMug_08_Phone.png"} alt=""/></Mobile>
          </div>
        </div>
      </div>
    );
  }
}

export default Product;
