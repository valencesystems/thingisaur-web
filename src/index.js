import React from 'react';
import ReactDOM from 'react-dom';
import {Switch, BrowserRouter, Route} from 'react-router-dom'

//import Order from './Order.js';
import './index.css';
import Main from './Main';
import Home from './Home';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <div>
        <BrowserRouter>
            <Switch>  
                <Route path="/product/:pid" component={Main}/>
                <Route path="/" component={Home}/>                
            </Switch>
        </BrowserRouter>
    </div>, document.getElementById('root'));

registerServiceWorker();
